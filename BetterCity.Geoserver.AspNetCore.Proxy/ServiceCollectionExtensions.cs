﻿using Microsoft.Extensions.DependencyInjection;
using ProxyKit;

namespace BetterCity.Geoserver.AspNetCore.Proxy
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddGeoserverProxy(this IServiceCollection services)
        {
            return services.AddProxy();
        }
    }
}