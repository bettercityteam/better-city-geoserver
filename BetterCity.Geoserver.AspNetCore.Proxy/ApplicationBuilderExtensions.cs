﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ProxyKit;

namespace BetterCity.Geoserver.AspNetCore.Proxy
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseGeoserverProxy(this IApplicationBuilder app)
        {
            var options = app.ApplicationServices.GetService<IOptions<GeoserverProxyOptions>>();
            return app.UseGeoserverProxy(options?.Value ?? new GeoserverProxyOptions());
        }

        public static IApplicationBuilder UseGeoserverProxy(this IApplicationBuilder app,
            GeoserverProxyOptions options)
        {
            return app.UseWhen(context => context.Request.Path.StartsWithSegments(options.BasePath), builder =>
            {
                builder.RunProxy(context => 
                    context
                        .ForwardTo(options.ForwardTo)
                        .AddXForwardedHeaders()
                        .Send());
            });
        }
    }
}