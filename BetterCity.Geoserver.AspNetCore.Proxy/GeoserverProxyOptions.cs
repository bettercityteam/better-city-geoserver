﻿namespace BetterCity.Geoserver.AspNetCore.Proxy
{
    public class GeoserverProxyOptions
    {
        public string BasePath { get; set; }
        
        public string ForwardTo { get; set; }
    }
}